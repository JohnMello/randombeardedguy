import BestFonts from "./images/100bestfonts.png";
import BestFontsAw from "./images/bestfonts2015.png";
import BigList from "./images/BigList.png";
import CollectUI from "./images/logoCollectui.png";
import Curators from "./images/Curators.png";
import Fontpair from "./images/logoFontpair.png";
import Typewolf from "./images/typewolf.png";
import TypoGoogle from "./images/logoGoogleComb.png";
import TypoGoogle2 from "./images/logoGoogleComb2.png";
import Flaticon from "./images/logo-flaticon.png";
import NounProject from "./images/logo-nounproject.png";
import Pixeden from "./images/logoPixeden.png";
import FreeMckp from "./images/logoFreemckp.png";
import GraphicBurguer from "./images/logo-graphicburguer.png";
import SiteInspire from "./images/logoSiteinspire.png";
import Mobbin from "./images/logoMobbin.png";


export const cardData = [
	{
		link:
			"https://www.creativebloq.com/graphic-design-tips/best-free-fonts-for-designers-1233380",
		image: BestFonts,
		title: "The 100 Best Free Fonts",
		meta: "By Tom May | February 20, 2019",
		description:
			"100 of the best free fonts on the internet divided in 8 categories."
	},
	{
		link:
			"https://www.awwwards.com/100-greatest-free-fonts-collection-for-2015.html",
		image: BestFontsAw,
		title: "The 100 Greatest Free Fonts Collection for 2015",
		meta: "By AWWWARDS | May,13 2015",
		description:
			"The 2015 version of The 100 Greatest Free Fonts Collection."
	},
	{
		link:
			"https://www.invisionapp.com/inside-design/free-typography-resources/",
		image: BigList,
		title: "Giant Repo of Typography Resources",
		meta: "By Shakti Sotomayor | Mar 10, 2017",
		description:
			"A list of the best free typography resources, from education to identification of fonts"
	},
	{
		link: "http://typ.io/",
		image: Curators,
		title: "Giant Repo of Typography Resources",
		meta: "Curator of Site's Fonts",
		description:
			"Revealing designer's decisions for all to see; peeking under the hood of beautiful websites to find out what fonts they're using and how they're using."
	}
];

export const cardDataPA = [
	{
		link: "https://www.typewolf.com/",
		image: Typewolf,
		title: "Font Pairing Curator",
		meta: "By Jeremiah Shoaf | 2013",
		description:
			"One of the best resources available for everything related to typography on the web."
	},
	{
		link: "https://fontpair.co/",
		image: Fontpair,
		title: "Font Pairing Curator for Google Fonts",
		meta: "",
		description:
			"Typography site dedicated to helping creators use beautiful typography for their creative projects."
	},
	{
		link:
			"https://www.behance.net/gallery/35768979/Typography-Google-Fonts-Combinations",
		image: TypoGoogle,
		title: "Typography: Google Fonts Combinations",
		meta: "Milo Themes, Mihai Baldean, Loredanna Papp-Dinea | Apr 5, 2016",
		description: "Inspiring typography pairing."
	},
	{
		link:
			"https://www.behance.net/gallery/41054815/Typography-Google-Fonts-Combinations-Volume-2",
		image: TypoGoogle2,
		title: "Typography: Google Fonts Combinations 2",
		meta: "Milo Themes, Mihai Baldean, Loredanna Papp-Dinea | Jul 26, 2016",
		description: "Inspiring typography pairing."
	}
];

export const cardDataPG = [
	{
		link:
			"https://www.flaticon.com/",
		image: Flaticon,
		title: "Flaticon",
		meta: "Freepikcompany",
		description: "The largest database of free icons available in PNG, SVG, EPS, PSD and BASE 64 formats."
	},{
		link:
			"https://thenounproject.com/",
		image: NounProject,
		title: "The Noun Project / Icons for everything",
		meta: "Noun Project Inc",
		description: "Over 2 Million curated icons, created by a global community."
	}
];

export const cardDataMK = [
	{
		link:
			"https://www.pixeden.com/",
		image: Pixeden,
		title: "Pixeden",
		meta: "",
		description: "Premium and free web and graphic design templates."
	},{
		link:
			"https://www.free-mockup.com/",
		image: FreeMckp,
		title: "Free Mock Up's",
		meta: "",
		description: "The biggest source of Free photorealistic Mockups online."
	},{
		link:
			"https://graphicburger.com",
		image: GraphicBurguer,
		title: "Graphic Burguer",
		meta: "",
		description: "Tasty design resources."
	}
];

export const cardDataUI = [
	{
		link:
			"https://mobbin.design",
		image: Mobbin,
		title: "Mobbin",
		meta: "Jiho Lim",
		description: "Hand-picked collection of the latest mobile design patterns from apps that reflect the best in design."
	},{
		link:
			"https://collectui.com",
		image: CollectUI,
		title: "Collect UI",
		meta: "Panda Network ",
		description: "Daily inspiration collected from ui archive and beyond."
	},{
		link:
			"https://www.siteinspire.com/",
		image: SiteInspire,
		title: "Site Inspire",
		meta: "Howells—Studio",
		description: "Showcase of the finest web and interactive design."
	}
];
