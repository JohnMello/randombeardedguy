import React from "react";
import ReactDOM from "react-dom";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import 'semantic-ui-css/semantic.min.css';
import App from "./App";
import Codex from "./components/pages/Codex";
import HeaderRbg from "./components/molecules/HeaderRbg";
import Featured from "./components/pages/Featured";
import Settings from "./components/pages/Settings";
import * as serviceWorker from "./serviceWorker";

const routing = (
  <Router>
    <HeaderRbg />
		<div className="ui container">
			<Route path="/" exact component={App} />
			<Route path="/codex" component={Codex} />
			<Route path="/featured" component={Featured} />
			<Route path="/settings" component={Settings} />
		</div>
		<div class="ui grey segment">
			<p class= "right">
				Version 1.0
			</p>
		</div>
  </Router>
)
ReactDOM.render(routing, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
