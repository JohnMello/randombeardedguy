import React from "react";

export default class Title extends React.Component {
	render() {
		return (
			<div style={{ color: "red" }}>
				<h2>{this.props.title}</h2>
			</div>
		);
	}
}
