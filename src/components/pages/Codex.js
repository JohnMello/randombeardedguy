import Card from "../molecules/Card";
import React from "react";
import Topic from "../atoms/Topic";
import {
	cardData,
	cardDataPA,
	cardDataPG,
	cardDataMK,
	cardDataUI
} from "../../assets/Datacodex";

export default class Codex extends React.Component {
	render() {
		const cardUnit = cardData.map(cardData => (
			<Card
				link={cardData.link}
				image={cardData.image}
				title={cardData.title}
				meta={cardData.meta}
				description={cardData.description}
			/>
		));

		const cardUnitPA = cardDataPA.map(cardDataPA => (
			<Card
				link={cardDataPA.link}
				image={cardDataPA.image}
				title={cardDataPA.title}
				meta={cardDataPA.meta}
				description={cardDataPA.description}
			/>
		));

		const cardUnitPG = cardDataPG.map(cardDataPG => (
			<Card
				link={cardDataPG.link}
				image={cardDataPG.image}
				title={cardDataPG.title}
				meta={cardDataPG.meta}
				description={cardDataPG.description}
			/>
		));

		const cardUnitMK = cardDataMK.map(cardDataMK => (
			<Card
				link={cardDataMK.link}
				image={cardDataMK.image}
				title={cardDataMK.title}
				meta={cardDataMK.meta}
				description={cardDataMK.description}
			/>
		));

		const cardUnitUI = cardDataUI.map(cardDataUI => (
			<Card
				link={cardDataUI.link}
				image={cardDataUI.image}
				title={cardDataUI.title}
				meta={cardDataUI.meta}
				description={cardDataUI.description}
			/>
		));

		return (
			<div>
				<h4>
					In here you will find an ever growing collection of
					resources to help you study and build your online projects
					from the ground up. The purpose of this is to put together
					all sort of materials found on the internet so that you can
					easily find whatever you need to bring your idea to life,
					from fonts to entire mock ups. <br />
					<br /> If you know more sites or projects that would be of
					great help to others, you can send the link to the email
					<span style={{ color: "#2185d0" }}>
						{" "}
						contact@randombeardedguy.com.br{" "}
					</span>
					with a small description and we will put it here.
				</h4>

				<Topic title="Font Inspiration" />

				<div class="ui stackable four column grid">{cardUnit}</div>

				<Topic title="Pairing Inspiration" />

				<div class="ui stackable four column grid">{cardUnitPA}</div>

				<Topic title="Pictograms" />

				<div class="ui stackable four column grid">{cardUnitPG}</div>

				<Topic title="Mock Up's" />

				<div class="ui stackable four column grid">{cardUnitMK}</div>

				<Topic title="UI Inspiration" />

				<div class="ui stackable four column grid">{cardUnitUI}</div>
			</div>
		);
	}
}
