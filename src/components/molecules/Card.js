import React from "react";

export default class Card extends React.Component {
	render() {
		return (
			<div class="column">
				<a href={this.props.link}>
					<div class="ui card">
						<img src={this.props.image} alt="Random-Beard-Dude" class="ui image" />
						<div class="content">
							<div class="header">{this.props.title}</div>
							<div class="meta">{this.props.meta}</div>
							<div class="description">
								{this.props.description}
							</div>
						</div>
						<div class="extra content">
							{/*<a>
								<i aria-hidden="true" class="user icon" />
								10 Friends
							</a>*/}
						</div>
					</div>
				</a>
			</div>
		);
	}
}
