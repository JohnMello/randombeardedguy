import React from "react";
import { Link } from "react-router-dom";
import Logo from "../../assets/images/Logo.png";

export default class HeaderRbg extends React.Component {
	render() {
		return (
			<div class="ui pointing secondary menu">
				<Link to="/">
					<div class="item">
						<img src={Logo} alt="Random-Bearded-Logo" />
					</div>
				</Link>
				<Link to="/codex">
					<a href=" " class="item">Codex</a>
				</Link>
			</div>
		);
	}
}
