import React from "react";
import LogoHome from "./assets/images/LogoHome.png";
import { Link } from "react-router-dom";


export default class App extends React.Component {
	constructor() {
		super();
		this.state = {
			title: "Home"
		};
	}

	changeTitle(title) {
		this.setState({ title });
	}
	render() {
		return (
			<div>
				<div class="ui one column grid">
					<div class="column">
						<div style={{ height: "100px" }} />
					</div>
				</div>
				<div class="ui one column grid">
					<div class="column">
						<Link to="/codex">
							<img
								src={LogoHome}
								class="ui centered circular image"
								alt="Random-Bearded-Guy"
							/>
						</Link>
					</div>
				</div>
				<div class="ui center aligned one column grid">
					<div class="column">
						<h3> Under Perpetual Construction </h3>
					</div>
				</div>

			</div>
		);
	}
}

